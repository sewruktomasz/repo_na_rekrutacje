-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 17, 2020 at 10:21 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `baza_na_rekrutacje`
--

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE `certificates` (
  `id_certificates` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `certificates`
--

INSERT INTO `certificates` (`id_certificates`, `name`, `description`) VALUES
(1, 'Cert 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis sem massa. Donec eu ligula massa. In iaculis nunc sem, nec auctor enim scelerisque elementum. '),
(2, 'Cert 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis sem massa. Donec eu ligula massa. In iaculis nunc sem, nec auctor enim scelerisque elementum. ');

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id_positions` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` longtext NOT NULL,
  `base_sallary` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id_positions`, `name`, `description`, `base_sallary`) VALUES
(1, 'Giermek', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis sem massa. Donec eu ligula massa. In iaculis nunc sem, nec auctor enim scelerisque elementum. ', 2600),
(2, 'Rycerz', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis sem massa. Donec eu ligula massa. In iaculis nunc sem, nec auctor enim scelerisque elementum. ', 3500);

-- --------------------------------------------------------

--
-- Table structure for table `workers`
--

CREATE TABLE `workers` (
  `id_workers` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `phone_no` varchar(15) NOT NULL,
  `id_positions` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `workers`
--

INSERT INTO `workers` (`id_workers`, `name`, `surname`, `phone_no`, `id_positions`) VALUES
(1, 'Jan', 'Kowalski', '789789789', 1),
(2, 'Wacław', 'Morawski', '987987987', 2);

-- --------------------------------------------------------

--
-- Table structure for table `workers_certificates`
--

CREATE TABLE `workers_certificates` (
  `id_workers_certificates` int(11) NOT NULL,
  `id_workers` int(11) NOT NULL,
  `id_certificates` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `workers_certificates`
--

INSERT INTO `workers_certificates` (`id_workers_certificates`, `id_workers`, `id_certificates`) VALUES
(1, 2, 1),
(2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `work_hours_register`
--

CREATE TABLE `work_hours_register` (
  `id_work_hours_register` int(11) NOT NULL,
  `datetime_work_start` datetime NOT NULL,
  `datetime_work_finish` datetime NOT NULL,
  `id_workers` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `work_hours_register`
--

INSERT INTO `work_hours_register` (`id_work_hours_register`, `datetime_work_start`, `datetime_work_finish`, `id_workers`) VALUES
(1, '2020-09-01 08:00:00', '2020-09-01 14:00:00', 1),
(2, '2020-09-09 18:18:21', '2020-09-09 22:18:21', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `certificates`
--
ALTER TABLE `certificates`
  ADD PRIMARY KEY (`id_certificates`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id_positions`);

--
-- Indexes for table `workers`
--
ALTER TABLE `workers`
  ADD PRIMARY KEY (`id_workers`),
  ADD KEY `id_positions` (`id_positions`);

--
-- Indexes for table `workers_certificates`
--
ALTER TABLE `workers_certificates`
  ADD PRIMARY KEY (`id_workers_certificates`),
  ADD KEY `id_workers` (`id_workers`),
  ADD KEY `id_certificates` (`id_certificates`);

--
-- Indexes for table `work_hours_register`
--
ALTER TABLE `work_hours_register`
  ADD PRIMARY KEY (`id_work_hours_register`),
  ADD KEY `id_workers` (`id_workers`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `certificates`
--
ALTER TABLE `certificates`
  MODIFY `id_certificates` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id_positions` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `workers`
--
ALTER TABLE `workers`
  MODIFY `id_workers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `workers_certificates`
--
ALTER TABLE `workers_certificates`
  MODIFY `id_workers_certificates` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `work_hours_register`
--
ALTER TABLE `work_hours_register`
  MODIFY `id_work_hours_register` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `workers`
--
ALTER TABLE `workers`
  ADD CONSTRAINT `workers_ibfk_1` FOREIGN KEY (`id_positions`) REFERENCES `positions` (`id_positions`);

--
-- Constraints for table `workers_certificates`
--
ALTER TABLE `workers_certificates`
  ADD CONSTRAINT `workers_certificates_ibfk_1` FOREIGN KEY (`id_workers`) REFERENCES `workers` (`id_workers`),
  ADD CONSTRAINT `workers_certificates_ibfk_2` FOREIGN KEY (`id_certificates`) REFERENCES `certificates` (`id_certificates`);

--
-- Constraints for table `work_hours_register`
--
ALTER TABLE `work_hours_register`
  ADD CONSTRAINT `work_hours_register_ibfk_1` FOREIGN KEY (`id_workers`) REFERENCES `workers` (`id_workers`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
