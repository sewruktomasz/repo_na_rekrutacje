﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Apka_rekrutacyjna
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void myInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            myOutput.Text = reverseString(myInput.Text);
        }

        private string reverseString(string text)
        {
            string result = "";
            char[] chars = text.ToCharArray();

            for(int i=chars.Length-1;i>=0; i--)
            {
                result += chars[i];
            }

            return result; 
        }

    }
}
